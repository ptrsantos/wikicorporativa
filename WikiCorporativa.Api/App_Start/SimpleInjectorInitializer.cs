﻿using Owin;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using WikiCorporativa.Infra.CrossCutting.IoC;

namespace WikiCorporativa.Api.App_Start
{
    public static class SimpleInjectorInitializer
    {
        public static Container Initialize(IAppBuilder app)
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            container.Options.AllowOverridingRegistrations = true;
            InitializeContainer(container, app);

            container.RegisterWebApiControllers(GlobalConfiguration.Configuration, Assembly.GetExecutingAssembly());
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            //DomainEvent.Container = new DomainEventsContainer(GlobalConfiguration.Configuration.DependencyResolver);

            return container;
        }


        private static void InitializeContainer(Container container, IAppBuilder app)
        {

            BootStrapperWikiCorporativa.Register(container, app, Lifestyle.Scoped);

        }
    }
}