﻿using System;
using System.Threading.Tasks;
using SimpleInjector;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using WikiCorporativa.Api.App_Start;

[assembly: OwinStartup(typeof(WikiCorporativa.Api.Startup))]

namespace WikiCorporativa.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {


                var container = SimpleInjectorInitializer.Initialize(app);

                //FilterConfig.RegisterGlobalFilters(GlobalConfiguration.Configuration.Filters, container);

                //ConfigureOAuthTokenConsumption(app);

                app.UseCors(CorsOptions.AllowAll);

        }
    }
}
