﻿using Owin;
using SimpleInjector;
using System;
using WikiCorporativa1.Data.Context;

namespace WikiCorporativa.Infra.CrossCutting.IoC
{
    public class BootStrapperWikiCorporativa
    {
        public static void Register(Container container, IAppBuilder app, Lifestyle lifeStyle)
        {
            container.RegisterInstance(app);

        //container.Register<IUnitOfWork, UnitOfWork>(lifeStyle);
        container.Register<WikiCorporativaContext>(lifeStyle);
        //container.Register<IDomainNotificationHandler<DomainNotification>, DomainNotificationHandler>(lifeStyle);
        }


    }
}
