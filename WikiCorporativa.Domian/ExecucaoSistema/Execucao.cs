﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikiCorporativa.Domian.ExecucaoSistema.Enums;

namespace WikiCorporativa.Domian.ExecucaoSistema
{
    public static class Execucao
    {
        public static TipoExecucao GetTipoExecucao(TipoExecucao force)
        {
            return force;
        }
        public static TipoExecucao GetTipoExecucao()
        {
#if DEBUG

            return TipoExecucao.Local;


#else
            return TipoExecucao.Producao;
#endif
        }
    }
}
