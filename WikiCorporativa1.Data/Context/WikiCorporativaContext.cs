﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using WikiCorporativa.Domian.ExecucaoSistema;
using WikiCorporativa.Domian.ExecucaoSistema.Enums;

namespace WikiCorporativa1.Data.Context
{


    public class WikiCorporativaContext : DbContext
    {
        private const string conexaoProducao = @"Data Source=SP7005SR600\PRODUCAO01;Initial Catalog=ControleDeOficioCI;user id=ownerControleDeOficioCI;password=ControleDeOficioCI7005#;persist security info=True;MultipleActiveResultSets=True";
        private const string conexaoLocal = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ControleDeOficioCI;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static TipoExecucao execucao = Execucao.GetTipoExecucao();

        public WikiCorporativaContext()
            : base(nameOrConnectionString: GetConnectionString())
        {
        }

        public static string GetConnectionString()
        {

            if (EhServidorWeb())
                return GetConnectionStringWeb();
            else
                return GetConnectionStringDesktop();
        }

        private static bool EhServidorWeb()
        {
            if (HttpRuntime.AppDomainAppId != null)
                return true;
            return false;
        }

        private static string GetConnectionStringDesktop()
        {
            if (execucao == TipoExecucao.Producao)
                return conexaoProducao;
            //if (execucao == TipoExecucao.Homologacao)
            //    return conexaoHomologacao;
            return conexaoLocal;
        }


        private static string GetConnectionStringWeb()
        {
            var regex = new Regex(@"^(localhost)|(127.0.0.1)|(www.cecvs.hom.sp.caixa)|(www.cecvs.sp.caixa)");

            Match match = regex.Match(HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Replace("http://", "").Replace("https://", ""));
            if (!match.Success)
                throw new Exception("Servidor web não reconhecido (web - match.success = 'false').");

            if (match.Groups[0].ToString() == "127.0.0.1" || match.Groups[0].ToString() == "localhost" || match.Groups[0].ToString() == "www.cecvs.hom.sp.caixa")
            {
                if (execucao == TipoExecucao.Producao)
                    return conexaoProducao;
                //if (execucao == TipoExecucao.Homologacao)
                //    return conexaoHomologacao;
                return conexaoLocal;
            }
            else if (match.Groups[0].ToString() == "www.cecvs.sp.caixa")
                return conexaoProducao;
            else
                throw new Exception("URL não reconhecida. Detalhes: " + match.Groups[0].ToString());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); //Desabilita Delete em cascata 1=>N
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>(); //Desabilita Delete em cascata N=>N
            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar").HasMaxLength(50));
        }

    }
}
